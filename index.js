//sifra i korisnicko ime za admina: admin




const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const path = require('path');
var xssFilter = require('x-xss-protection')
const bcrypt = require('bcrypt');
const fs = require('fs');
const Sequelize = require('sequelize');

var app = express();
app.use(bodyParser.json());
app.use(xssFilter());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/files", express.static(path.join(__dirname, "public")));

app.use(session({
  secret: "whatIfIToldYouThatTheSecretIsSecretLameIKnow",
  resave: true,
  saveUninitialized: true
}));

const sequelize = new Sequelize('sql11217651', 'sql11217651', 't5A16indW5', {
  host:'sql11.freemysqlhosting.net',
  dialect:'mysql',
  port: 3306,
  pool: {
    max: 5,
    min:0,
    aquire:30000,
    idle:10000
  }
});

//modeli
var Users = sequelize.define('korisnici', {
    username: Sequelize.STRING,
    password: Sequelize.STRING,
    podaci: Sequelize.INTEGER,
    role: Sequelize.INTEGER
});

var Roles = sequelize.define('role', {
    roleName: Sequelize.STRING
});

var Data = sequelize.define('licniPodaci', {
    ime: Sequelize.STRING,
    indeks: Sequelize.INTEGER,
    email: Sequelize.STRING,
    akademskaGodina: Sequelize.STRING,
    brojGrupe: Sequelize.STRING,
    maxGrupa: Sequelize.INTEGER,
    semestar: Sequelize.INTEGER,
    Repozitorij: Sequelize.STRING,
    BBURL: Sequelize.STRING,
    BBSSH: Sequelize.STRING,
    regex: Sequelize.STRING,
    verified: Sequelize.BOOLEAN
});

Users.sync();
Roles.sync();
Data.sync();


Roles.findOne({where:{roleName: "Admin"}})
    .then(admin => {
      let nextId;
     if(!admin){
            Roles.create({roleName: "Admin"}).then( role => {
              nextId = role.dataValues.id;
                Users.create({
                    username: "admin",
                    password: bcrypt.hashSync("admin", 10),
                    podaci: nextId,
                    role: nextId
                  }).then(temp => Data.create({
                      ime: null,
                      indeks: null,
                      email: null,
                      akademskaGodina: null,
                      brojGrupe: null,
                      maxGrupa: null,
                      semestar: null,
                      Repozitorij: null,
                      BBURL: null,
                      BBSSH: null,
                      regex: null,
                      verified: null
                  }));

            });
            
        }
        
});

app.get('/',function(req,res){
    let sadrzaj = "";
    sadrzaj += '<!DOCTYPE html><head><title>WT Spirala</title><link rel="stylesheet" type="text/css" href="/files/common.css"><link rel="stylesheet" type="text/css" href="/files/login.css"><link rel="stylesheet" type="text/css" href="/files/unoskomentara.css"><link rel="stylesheet" type="text/css" href="/files/statistika.css"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></head>';
    sadrzaj += '<script src="/files/Navigacija.js"></script><script src="/files/validacija.js"></script><script src="/files/generisi.js"></script>';
    sadrzaj += '<script src="/files/login.js"></script><script src="/files/poruke.js"></script><script src="/files/kreirajFajl.js"></script><script src="/files/bitbucketApi.js"></script><script src="/files/unoskomentara.js"></script><script src="/files/listaKorisnika.js"></script>';
    
    if (req.session.who == null){
      sadrzaj += '<body><div id="meni"><div class="logo"><img id="slika" src="https://megatron.sk/wp-content/uploads/2014/12/html-css-js.jpg" alt="WT Spirala"></div>';
      sadrzaj += '<ul class="meniLista"><li id="pocetna" onclick="Navigacija.idiNaLogin()">Pocetna</li><li id="deploy"><a href="http://wtspirala4-wtprojekat17260.1d35.starter-us-east-1.openshiftapps.com">Deploy</a></li></ul></div><div><h6 id="PObavijest">Obavijest: </h6></div><div id="kontejner"></div></body>';
    }else if(req.session.who == "Student"){
      sadrzaj += '<body><div id="meni"><div class="logo"><img id="slika" src="https://megatron.sk/wp-content/uploads/2014/12/html-css-js.jpg" alt="WT Spirala"></div>';
      sadrzaj += '<ul class="meniLista"><li id="odjava" onclick="odjavi()">Odjava</li><li id="statistika" onclick="Navigacija.idiNaStatistiku()">Statistika</li>'+
			'<li id="unoskomentara" onclick="Navigacija.idiNaUnosKomentara()">Unos komentara</li></ul></div><div><h6 id="PObavijest">Obavijest: </h6></div><div id="kontejner"></div></body>';
    }else if(req.session.who == "Nastavnik"){
      sadrzaj += '<body><div id="meni"><div class="logo"><img id="slika" src="https://megatron.sk/wp-content/uploads/2014/12/html-css-js.jpg" alt="WT Spirala"></div>';
      sadrzaj += '<ul class="meniLista"><li id="odjava" onclick="odjavi()">Odjava</li><li id="unosspiska" onclick="Navigacija.idiNaUnosSpiska()">Unos spiska</li>';
			sadrzaj += '<li id="nastavnik" onclick="Navigacija.idiNaNastavnik()">Nastavnik</li><li id="BBpozivi" onclick="Navigacija.idiNaBBpozivi()">Bitbucket pozivi</li>'+
			'</ul></div><div><h6 id="PObavijest">Obavijest: </h6></div><div id="kontejner"></div></body>';
    }else if(req.session.who == "Admin"){
      sadrzaj += '<body><div id="meni"><div class="logo"><img id="slika" src="https://megatron.sk/wp-content/uploads/2014/12/html-css-js.jpg" alt="WT Spirala"></div>';
      sadrzaj += '<ul class="meniLista"><li id="odjava" onclick="odjavi()">Odjava</li><li id="administracija" onclick="Navigacija.idiNaAdministraciju()">Administracija</li>';
			sadrzaj += '</ul></div><div><h6 id="PObavijest">Obavijest: </h6></div><div id="kontejner"></div></body>';
    }
    res.send(sadrzaj);
  
});



app.get('/login',function(req,res){
  res.sendFile(__dirname + '/public/login.html');
});

app.get('/statistika',function(req,res){
  res.sendFile(__dirname + '/public/statistika.html');
});

app.get('/unoskomentara',function(req,res){
  res.sendFile(__dirname + '/public/unoskomentara.html');
});

app.get('/unosSpiska',function(req,res){
  res.sendFile(__dirname + '/public/unosSpiska.html');
});

app.get('/nastavnik',function(req,res){
  res.sendFile(__dirname + '/public/nastavnik.html');
});

app.get('/BBpozivi',function(req,res){
  res.sendFile(__dirname + '/public/bitbucketPozivi.html');
});

app.get('/Administracija',function(req,res){
  res.sendFile(__dirname + '/public/listaKorisnika.html');
});








//5a
app.post('/komentar', function(req, res) {

  //provjera

  let temp = JSON.parse(req.body.sadrzaj);

  if (req.body.spirala==null || req.body.spirala.length==0 || req.body.index==null || req.body.index.length==0 || req.body.sadrzaj==null || req.body.sadrzaj.length==0) {
    res.message="Podaci nisu u trazenom formatu!";
    res.data=null;
    res.status(400).send("Podaci nisu u trazenom formatu!");
    return;
  }

  if (temp.sadrzaj.length==0) {
    res.message="Podaci nisu u trazenom formatu!";
    res.data=null;
    res.status(400).send("Podaci nisu u trazenom formatu!");
    return;
  }

  for (let i=0; i<temp.sadrzaj.length; i++) {
    if (!temp.sadrzaj[i].hasOwnProperty('ocjena') || !temp.sadrzaj[i].hasOwnProperty('tekst') || !temp.sadrzaj[i].hasOwnProperty('sifra_studenta')){
      res.message="Podaci nisu u trazenom formatu!";
      res.data=null;
      res.status(400).send("Podaci nisu u trazenom formatu!");
      return;
    }
  }

  fs.writeFile("/public/" + "markS" + req.body.spirala + req.body.index + ".json", req.body.sadrzaj, function(error) {
    if (error) {
      res.status(400).send("Error");
    }
  });

  res.message="Uspjesno kreirana datoteka!";
  res.data=req.body.sadrzaj;
  res.status(200).send("Uspjesno kreirana datoteka!");
});

//5b
app.post('/lista', function(req, res) {

  if (req.body.godina==undefined || req.body.nizRepozitorija==undefined || req.body.godina==null || req.body.godina.length==0  || req.body.nizRepozitorija==null || req.body.nizRepozitorija.length==0) {
    res.message="Podaci nisu u trazenom formatu!";
    res.data=null;
    return;
  }

  var finalni=[];
  for (let i=0; i<req.body.nizRepozitorija.length; i++) {
    let ime=req.body.nizRepozitorija[i];
    if (ime.includes(req.body.godina)){ 
      finalni.push(ime); 
    }
  }

  fs.writeFile("/public/" + "spisak" + req.body.godina +  ".txt", JSON.stringify(finalni, null, 4), function(error) {
    if (error) {
      res.status=400;
      res.status(400).send("Lista nije uspjesno kreirana!");
    }
    res.status=200;
    res.message="Lista uspjesno kreirana";
    res.data=finalni.length;
  });

  res.message="Lista uspjesno kreirana!";
  res.data=finalni.length; //tj broj redova datoteke
  res.status(200).send("Lista uspjesno kreirana! Broj redova: "+finalni.length);
});


//5C
app.post('/izvjestaj', function(req, res) {

  var autori=[];
  var pozicijeJ=[];
  var brojac=0;
  var listaDatoteka = fs.readdir;
  if (!fs.existsSync('/public/SpisakS'+req.body.spirala+'.json')) {
    res.message='Datoteka '+'/public/SpisakS'+req.body.spirala+'.json ne postoji.';
    res.status(400).send('Datoteka '+'/public/SpisakS'+req.body.spirala+'.json ne postoji.');
    return;
  }
  var json = JSON.parse(fs.readFileSync('/public/SpisakS' + req.body.spirala + '.json', 'utf8'));
  for (let i=0; i<json.length; i++) {
    var red = json[i];
    for (let j=1; j<6; j++) {
      if (red[j]==req.body.index) {
        autori[brojac]=red[0];
        pozicijeJ[brojac]=j;
        brojac=brojac+1;
      }
    }
  }

  var sviKomentari=[];

  for (let i=0; i<autori.length; i++) {
    if (!fs.existsSync('/public/markS'+req.body.spirala+autori[i]+'.json')) {
      sviKomentari.push("");
      continue;
    }
    var temp = JSON.parse(fs.readFileSync('/public/markS' + req.body.spirala + autori[i]+ '.json', 'utf8'));
    switch(pozicijeJ[i]) {
      case 0:
      //trazi A
      t=temp.sadrzaj[i];
      sviKomentari.push(t.tekst);
      break;

      case 1:
      t=temp.sadrzaj[i];
      sviKomentari.push(t.tekst);
      break;

      case 2:
      t=temp.sadrzaj[i];
      sviKomentari.push(t.tekst);
      break;

      case 3:
      t=temp.sadrzaj[i];
      sviKomentari.push(t.tekst);
      break;

      case 4:
      t=temp.sadrzaj[i];
      sviKomentari.push(t.tekst);
      break;

      case 5:
      t=temp.sadrzaj[i];
      sviKomentari.push(t.tekst);
      break;
    }
  }



  fs.writeFile("/public/" + "izvjestajS" + req.body.spirala + req.body.index + ".txt", JSON.stringify(sviKomentari, null, '#########\n'), function(error) {
    if (error) {
      res.status=400;
    }
    res.status=200;
  });
  //res.download("/static/" + "izvjestajS" + req.body.spirala + req.body.index + ".txt", "izvjestajS"+req.body.spirala+req.body.index+".txt");
  res.message="Uspjesno kreirana datoteka!";
  res.data=JSON.stringify(req.body.komentar, null, 4);
  res.status(200).send("Uspjesno kreirana datoteka!");
});


//5D
app.post('/bodovi', function(req, res) {

  var autori=[];
  var pozicijeJ=[];
  var brojac=0;
  if (!fs.existsSync('/public/SpisakS'+req.body.spirala+'.json')) {
    res.message='Datoteka '+'/public/SpisakS'+req.body.spirala+'.json ne postoji.';
    res.status(404).send('Datoteka '+'/public/SpisakS'+req.body.spirala+'.json ne postoji.');
    return;
  }
  var json = JSON.parse(fs.readFileSync('/public/SpisakS' + req.body.spirala + '.json', 'utf8'));
  for (let i=0; i<json.length; i++) {
    var red = json[i];
    for (let j=1; j<6; j++) {
      if (red[j]==req.body.index) {
        autori[brojac]=red[0];
        pozicijeJ[brojac]=j;
        brojac=brojac+1;
      }
    }
  }
  var sviKomentari=[];

  for (let i=0; i<autori.length; i++) {
    if (!fs.existsSync('/public/markS'+req.body.spirala+autori[i]+'.json')) {
      continue;
    }
    var temp = JSON.parse(fs.readFileSync('/public/markS' + req.body.spirala + autori[i]+ '.json', 'utf8'));
    switch(pozicijeJ[i]) {
      case 0:
      //trazi A
      t=temp.sadrzaj[i];
      sviKomentari.push(t.ocjena);
      break;

      case 1:
      t=temp.sadrzaj[i];
      sviKomentari.push(t.ocjena);
      break;

      case 2:
      t=temp.sadrzaj[i];
      sviKomentari.push(t.ocjena);
      break;

      case 3:
      t=temp.sadrzaj[i];
      sviKomentari.push(t.ocjena);
      break;

      case 4:
      t=temp.sadrzaj[i];
      sviKomentari.push(t.ocjena);
      break;

      case 5:
      t=temp.sadrzaj[i];
      sviKomentari.push(t.ocjena);
      break;
    }
  }

  let ukupno=0;
  for (let i=0; i<sviKomentari.length; i++) {
    ukupno=ukupno+sviKomentari[i];
  }
  
  ukupno=Math.floor(ukupno/sviKomentari.length)+1;
  res.status(200).send('Student '+req.body.index+" je u prosjeku ostvario "+ukupno+" mjesto.");
});


//4 
app.post('/unosspiska', function(req, res) {
  let redovi = req.body.redovi;
  for (var i=0; i<redovi.length; i++) {
    kolone=redovi[i].split(',');
    if (kolone.length!=6) {
      res.message="Greska u redu "+i;
      res.status(400).send("Greska u redu "+i);
      return;
    }
    for (var j=0; j<6; j++) {
      for (var k=j+1; k<6; k++) {
        if (kolone[j]==kolone[k]) {
          res.message="Greska u redu "+i+". Postoji isti indeks u tom redu.";
          res.status(400).send("Greska u redu "+i+". Postoji isti indeks u tom redu.");
          return;
        }
      }
    }
  }

  for (var j=0; j<redovi.length; j++) {
    redovi[j]=redovi[j].split(',');
  }

  fs.writeFile("/public/" + "SpisakS" + req.body.spirala + ".json", JSON.stringify(req.body.redovi, null, 4), function(error) {
    if (error) {
      res.message="Datoteka nije kreirana";
      res.status(400).send("Datoteka nije kreirana");
    }
      res.message="Datoteka je kreirana";
      res.status(200).send("Datoteka je kreirana");
  });

});


//IV spirala


function validirajS(data) {

  return true;
}

function validirajP(data) {
  //validacija za svaki
  return true;
}


app.post('/studentRegistracija', function(req, res) {
   let nextId;
   console.log(req.body);
   if(req.body['ime']&&req.body['email']&&req.body['indeks']&&req.body['odaberiGrupu'] &&
    req.body['password']&&req.body['passwordPotvrda']&&req.body['akgodina']&&req.body['bburl'] &&
    req.body['bbssh']&&req.body['repozitorij'] && validirajS(req.body)){
         Roles.create({roleName: "Student"}).then( role => {nextId = role.dataValues.id;
                Users.create({
                    username: req.body['ime'].split(" ")[0] + req.body['indeks'],
                    password: bcrypt.hashSync(req.body['password'], 10),
                    podaci: nextId,
                    role: nextId
                  }).then(temp => Data.create({
                      ime: req.body['ime'],
                      indeks: req.body['indeks'],
                      akademskaGodina: req.body['akgodina'],
                      brojGrupe: req.body['odaberiGrupu'],
                      Repozitorij: req.body['repozitorij'],
                      email: req.body['email'],
                      maxGrupa: null,
                      BBURL: req.body['bburl'],
                      BBSSH: req.body['bbssh'],
                      regex: null,
                      semestar: null,
                      verified: null
                  }));

            });
      res.json({poruka: "Vase korisnicko ime je "+req.body['ime'].split(" ")[0] + req.body['indeks']});
    } else {
      res.json({poruka: "Validacija nije prosla!"});
    }
});


app.post('/profesorRegistracija', function(req, res) {
    let nextId;
    //ajax.send(JSON.stringify({ime: imeP, email:mail, password:pass, passwordPotvrda:potvrda, akgodina:godina, semestar:odabrano, korisnickoIme: korisIme, maxBrojGrupa:brojG, regex:regex}));

   if(req.body['ime']&&req.body['email']&&req.body['semestar']&&req.body['korisnickoIme'] &&
    req.body['password']&&req.body['passwordPotvrda']&&req.body['akgodina']&&req.body['regex'] &&
    req.body['maxBrojGrupa'] && validirajP(req.body)){
         Roles.create({roleName: "Nastavnik"}).then( role => {nextId = role.dataValues.id;
                Users.create({
                    username: req.body['korisnickoIme'],
                    password: bcrypt.hashSync(req.body['password'], 10),
                    podaci: nextId,
                    role: nextId
                  }).then(temp => Data.create({
                      ime: req.body['ime'],
                      indeks: null,
                      akademskaGodina: req.body['akgodina'],
                      brojGrupe: null,
                      Repozitorij: null,
                      email: req.body['email'],
                      maxGrupa: req.body['maxBrojGrupa'],
                      BBURL: null,
                      BBSSH: null,
                      regex: req.body['regex'],
                      semestar: req.body['semestar'],
                      verified: false
                  }));

            });
      res.json({poruka: "Prof "+req.body['ime']});
    } else {
      res.json({poruka: "Validacija nije prosla!"});
    }
});


app.post('/prijavi', function(req, res) {

  var username = req.body['username'];
  var pass = req.body['password'];
  Users.findOne({where: {username: username}})
  .then(rez => {
      if(rez == null) { 
        res.json({poruka: "Neispravni podaci!"});
      }
      if (bcrypt.compareSync(req.body['password'],rez.dataValues.password)){
          Roles.findOne({where: {id: rez.dataValues.role}}).then(r => {
            if(r != null) { 
              if(r.dataValues.roleName != "Nastavnik") {
                req.session.who = r.dataValues.roleName; 
                res.json({poruka: "Uspjesno ste se prijavili!"});
              } else {
                //provjeri je li verified

                Data.findOne({where: {id: rez.dataValues.role}}).then(d => {
                    if (d.dataValues.verified==true) {
                      req.session.who = r.dataValues.roleName; 
                      res.json({poruka: "Uspjesno ste se prijavili!"});
                    } else {
                      res.json({poruka: "Niste verified! Obratite se administratoru"});
                    }
                });

              }
            }
          });
      } 
      else{
          res.json({poruka: "Neispravni podaci!"});
      }
  }).catch(rez =>  console.log(rez));
  //izbrisati gore catch
});

app.get('/odjavi',function(req,res){
  req.session.who = null;
  res.json({poruka: "Uspjesno ste se odjavili!"});
});

app.post('/pretrazi', function(req,res){
    var data = [];
    var idKorisnika=-1;
    var polje = req.body.polje;
    Users.findOne({where: {username:polje}}).then(rez => {
      if (rez!=null) {
        idKorisnika = rez.dataValues.id;
        Data.findOne({where: {id:idKorisnika}}).then(r=> {
          if (r!=null) {
            res.json({korisnik: rez.dataValues.username, verif : r.dataValues.verified, ID:idKorisnika});
          } else {
            res.json({poruka: "Doslo je do greske!"});
          }
        });
      } else {
        res.json({poruka: "Nema tog korisnika!"});
      }
    });
});



app.post('/verifikuj', function(req, res) {
  var ID = req.body.ID;
  Data.update( {verified:true}, {where: {id:ID}}).then(rez => {
    if (rez!=null) {
      res.json({poruka: "OK"});
    } else {
      res.json({poruka: "GRESKA"});
    }
  });

});

app.post('/unverifikuj', function(req, res) {

  var ID = req.body.ID;
  Data.update( {verified:false}, {where: {id:ID}}).then(rez => {
    if (rez!=null) {
      res.json({poruka: "OK"});
    } else {
      res.json({poruka: "GRESKA"});
    }
  });

});

app.listen(3000);