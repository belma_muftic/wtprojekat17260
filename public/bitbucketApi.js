var BitbucketApi = (function(){
	

	var dohvatiAccessToken = (function(key, secret, fnCallback) {
		 var ajax = new XMLHttpRequest();
		if (key==null || secret==null || key==undefined || secret==undefined || key=="" || secret=="") {
			fnCallback(-1, "Key ili secret nisu pravilno proslijedjeni!");
			return;
		}

		ajax.onreadystatechange = function() {
			if (ajax.readyState == 4 && ajax.status==200) {
			   tokenF=JSON.parse(ajax.responseText).access_token;
			   fnCallback(null, JSON.parse(ajax.responseText).access_token);
			} else if (ajax.readyState==4) {
			   fnCallback(ajax.status, null)
			}
		}

		ajax.open("POST", "https://bitbucket.org/site/oauth2/access_token", false);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajax.setRequestHeader("Authorization", 'Basic ' + btoa(key+':'+secret));
		ajax.send("grant_type="+encodeURIComponent("client_credentials"));

	});

	var dohvatiRepozitorije = (function(token, godina, naziv, branch, fnCallback) {
		var ajax = new XMLHttpRequest();
		//u nazivu sadrze naziv X
		//respos kreirani u godini, ili godina+1 X
		//mora imati branch X
		//data da ima listu svih ssh
		ajax.onreadystatechange = function() {
		 	if (ajax.readyState == 4 && ajax.status==200) {
				var pglen = JSON.parse(ajax.responseText).pagelen;
		 		var podaci = JSON.parse(ajax.responseText).values;
				var repos=[];
				var brojac=0;
				var naredna=godina+1;
				godina=godina.toString();
				naredna=naredna.toString();
		 		for (let i=0; i<pglen; i++) {
					 repos[i]=JSON.parse(ajax.responseText).values[i];
				 }
				 
				 var finalRepos = [];
				 for (let i=0; i<repos.length; i++) {
					 if (repos[i]==undefined) {
						break;
					 }
					if (!(repos[i].name).includes(naziv)){
						continue;
					}
					
					if( (repos[i].created_on).substr(0,4)!=godina && (repos[i].created_on).substr(0,4)!=naredna ) {
						continue;
					}
					finalRepos[brojac]=repos[i];
					brojac=brojac+1;
				 }
				 fnCallback(null,finalRepos);
		 	} else {
				fnCallback(ajax.status, null)
		 	}
		 }

		 ajax.open("GET", "https://api.bitbucket.org/2.0/repositories?role=member&pagelen=150",false);
		 ajax.setRequestHeader("Authorization", 'Bearer '+token);
		 ajax.send();


	});

	var dohvatiBranch = (function(token, url, naziv, fnCallback) {
		var ajax = new XMLHttpRequest();

		ajax.onreadystatechange = function(){
			if (ajax.readyState==4 && ajax.status==200) {
				var podaci = JSON.parse(ajax.responseText).values;
				for (let i=0; i<podaci.length; i++) {
					if (podaci[i].type=='branch' && (podaci[i].name).includes(naziv)) {
						fnCallback(null, true);
						return;
					}
				}
				fnCallback(null, false);
			} else if (ajax.readyState==4) {
				fnCallback(ajax.status,null);
			}
		}
		
		ajax.open("GET", url, false);
		ajax.setRequestHeader("Authorization", 'Bearer '+token);
		ajax.send();
	});

    return {
        dohvatiAccessToken: dohvatiAccessToken,
        dohvatiRepozitorije: dohvatiRepozitorije,
        dohvatiBranch: dohvatiBranch
    }
})();
