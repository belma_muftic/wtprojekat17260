var Generisi = (function(){

    generisiIzvjestaj = (function() {
        var indeks = document.getElementById('indeks').value;
        var spirala = document.getElementById('spirala').value;
        //var obavijest = document.getElementById('PObavijest').innerHTML;
        KreirajFajl.kreirajIzvjestaj(spirala, indeks, function(error, data) {document.getElementById('PObavijest').innerHTML=("Obavijest: Error - "+error+"\nData - "+data);});
    });

    generisiBodove = (function() {
        var indeks = document.getElementById('indeks').value;
        var spirala = document.getElementById('spirala').value;
        KreirajFajl.kreirajBodove(spirala, indeks, function(error, data) {document.getElementById('PObavijest').innerHTML=("Obavijest: Error - "+error+"\nData - "+data);});
    });

    /*generisiListu = (function() {
        var key = document.getElementById('Key').value;
        var secret = document.getElementById('Secret').value;
        var naziv = document.getElementById('Naziv').value;
        var branch = document.getElementById('Branch').value;
        var trGodina = document.getElementById('trenutnaGodina').value;
        KreirajFajl.generisiListu(trGodina, nizRepos, function(error, data) {alert(error+" "+data);});
    });*/


    generisiKomentar = (function(){
        var sifra1 = document.getElementById('sifra1').innerHTML;
        var sifra2 = document.getElementById('sifra2').innerHTML;
        var sifra3 = document.getElementById('sifra3').innerHTML;
        var sifra4 = document.getElementById('sifra4').innerHTML;
        var sifra5 = document.getElementById('sifra5').innerHTML;

        var ocjenaA=0;
        var ocjenaB=0;
        var ocjenaC=0;
        var ocjenaD=0;
        var ocjenaE=0;
        var komA="";
        var komB="";
        var komC="";
        var komD="";
        var komE="";

        if (sifra1=="A") {
            ocjenaA=0;
            komA = document.getElementById('komS1').value;
        } else if (sifra1=="B") {
            ocjenaB=0;
            komB = document.getElementById('komS1').value;
        } else if (sifra1=="C") {
            ocjenaC=0;
            komC = document.getElementById('komS1').value;
        } else if (sifra1=="D") {
            ocjenaD=0;
            komD = document.getElementById('komS1').value;
        } else {
            ocjenaE=0;
            komE = document.getElementById('komS1').value;
        }
        if (sifra2=="A") {
            ocjenaA=1;
            komA = document.getElementById('komS2').value;
        } else if (sifra2=="B") {
            ocjenaB=1;
            komB = document.getElementById('komS2').value;
        } else if (sifra2=="C") {
            ocjenaC=1;
            komC = document.getElementById('komS2').value;
        } else if (sifra2=="D") {
            ocjenaD=1;
            komD = document.getElementById('komS2').value;
        } else {
            ocjenaE=1;
            komE = document.getElementById('komS2').value;
        }
        if (sifra3=="A") {
            ocjenaA=2;
            komA = document.getElementById('komS3').value;
        } else if (sifra3=="B") {
            ocjenaB=2;
            komB = document.getElementById('komS3').value;
        } else if (sifra3=="C") {
            ocjenaC=2;
            komC = document.getElementById('komS3').value;
        } else if (sifra3=="D") {
            ocjenaD=2;
            komD = document.getElementById('komS3').value;
        } else {
            ocjenaE=2;
            komE = document.getElementById('komS3').value;
        }
        if (sifra4=="A") {
            ocjenaA=3;
            komA = document.getElementById('komS4').value;
        } else if (sifra4=="B") {
            ocjenaB=3;
            komB = document.getElementById('komS4').value;
        } else if (sifra4=="C") {
            ocjenaC=3;
            komC = document.getElementById('komS4').value;
        } else if (sifra4=="D") {
            ocjenaD=3;
            komD = document.getElementById('komS4').value;
        } else {
            ocjenaE=3;
            komE = document.getElementById('komS4').value;
        }
        if (sifra5=="A") {
            ocjenaA=4;
            komA = document.getElementById('komS5').value;
        } else if (sifra5=="B") {
            ocjenaB=4;
            komB = document.getElementById('komS5').value;
        } else if (sifra5=="C") {
            ocjenaC=4;
            komC = document.getElementById('komS5').value;
        } else if (sifra5=="D") {
            ocjenaD=4;
            komD = document.getElementById('komS5').value;
        } else {
            ocjenaE=4;
            komE = document.getElementById('komS5').value;
        }


        var link1 = document.getElementById('link1').value;
        var link2 = document.getElementById('link2').value;
        var link3 = document.getElementById('link3').value;
        var link4 = document.getElementById('link4').value;
        var link5 = document.getElementById('link5').value;

        var spirala = document.getElementById('brojSpirale').value;
        var index = document.getElementById('index').value;
        var sadrzaj = { 'sadrzaj':
            [{'sifra_studenta' : "A", 'tekst' : komA, 'ocjena' : ocjenaA},
            {'sifra_studenta' : "B", 'tekst' : komB, 'ocjena' : ocjenaB},
            {'sifra_studenta' : "C", 'tekst' : komC, 'ocjena' : ocjenaC},
            {'sifra_studenta' : "D", 'tekst' : komD, 'ocjena' : ocjenaD},
            {'sifra_studenta' : "E", 'tekst' : komE, 'ocjena' : ocjenaE}]
        };

        sadrzaj = JSON.stringify(sadrzaj);
//{‘spirala’:’1’,’index’:’12345’,’sadrzaj’:[{‘sifra_studenta’:’A’,’tekst’:’Neki tekst’,’ocjena’:0},...]},
        KreirajFajl.kreirajKomentar(spirala, index, sadrzaj, function(error, data) {document.getElementById('PObavijest').innerHTML=("Obavijest: Error - "+error+"\nData - "+data);});
    });

    generisiToken = (function(){
        var key = document.getElementById('Key').value;
        var secret = document.getElementById('Secret').value;
        var naziv = document.getElementById('Naziv').value;
        var branch = document.getElementById('Branch').value;
        var trGodina = document.getElementById('trenutnaGodina').value;
        var token="";
        var repos=[];
        var postojiBranch=[];
        var finalRepos=[];
        var greska=0;
        branch=branch+"";
        BitbucketApi.dohvatiAccessToken(key, secret, function(error, data){document.getElementById('PObavijest').innerHTML=("Obavijest: Error - "+error+"\nData - "+data); greska=error; token=data});
        if (greska==-1) {
            return;
        }
        BitbucketApi.dohvatiRepozitorije(token, trGodina, naziv, branch, function(error, data){document.getElementById('PObavijest').innerHTML=("Obavijest: Error - "+error+"\nData - "+data); repos=data;});
        for (let i=0; i<repos.length; i++) {
            BitbucketApi.dohvatiBranch(token, repos[i].links.branches.href,branch,function(error, data) { postojiBranch.push(data);});
        }
        for (let i=0; i<postojiBranch.length; i++) {
            if (postojiBranch[i]) {
                finalRepos.push(repos[i].links.clone[1].href);
            }
        }
        KreirajFajl.kreirajListu(trGodina, finalRepos, function(error, data) {document.getElementById('PObavijest').innerHTML=("Obavijest: Error - "+error+"\nData - "+data);});
    });

    return {
        generisiIzvjestaj : generisiIzvjestaj,
        generisiBodove : generisiBodove,
        generisiKomentar : generisiKomentar,
        generisiToken : generisiToken
    }


}());