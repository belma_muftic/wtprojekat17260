var KreirajFajl=(function(){

	kreirajKomentar = (function(spirala, index, sadrzaj, fnCallback) {

		if (spirala.length==0 || index.length==0) {
			fnCallback(-1, "Neispravni parametri");
			return;
		}
		var temp=JSON.parse(sadrzaj);
		if (temp.sadrzaj.length==0 || !temp.sadrzaj[0].hasOwnProperty('ocjena') || !temp.sadrzaj[0].hasOwnProperty('sifra_studenta') || !temp.sadrzaj[0].hasOwnProperty('tekst')) {
			fnCallback(-1, "Neispravni parametri");
			return;
		}

		/* {‘spirala’:’1’,’index’:’12345’,’sadrzaj’:[{‘sifra_studenta’:’A’,’tekst’:’Neki tekst’,’ocjena’:0},...]} */
		var ajax = new XMLHttpRequest();
		ajax.onreadystatechange = function() {
			if (ajax.status==200 && ajax.readyState==4) {
				fnCallback(null, ajax.responseText);
			} else if (ajax.readyState==4) {
				fnCallback(ajax.status, ajax.responseText);
			}

		}

		ajax.open("POST", "/komentar", true);
		ajax.setRequestHeader('Content-Type', 'application/json');
		ajax.send(JSON.stringify({spirala:spirala,index:index,sadrzaj: sadrzaj}));
	});


	kreirajListu = (function(godina, nizRepozitorija, fnCallback) {

		var ajax = new XMLHttpRequest();
		var godinaS=godina.toString();
		if (godinaS.length==0 || godinaS==null || nizRepozitorija.length==0 || nizRepozitorija==null) {
			fnCallback(-1, "Neispravni parametri");
			return;
		}
		ajax.onreadystatechange = function() {
			if (ajax.status==200 && ajax.readyState==4) {
				fnCallback(null, ajax.responseText);
			} else if (ajax.readyState==4) {
				fnCallback(ajax.status, ajax.responseText);
			}
		}

		ajax.open("POST", "lista", true);
		ajax.setRequestHeader('Content-Type', 'application/json');
		ajax.send(JSON.stringify({godina:godinaS, nizRepozitorija:nizRepozitorija}));
	});

	kreirajIzvjestaj = (function(spirala, index, fnCallback) {
		var spiralaS="";
		var indexS="";
		if (typeof(spirala)!='string') {
			 spiralaS=spirala.toString();
		} else {
			spiralaS=spirala;
		}
		if (typeof(index)!='string') {
			indexS=index.toString();
		} else {
			indexS=index;
		}

		if (spiralaS.length==0 || indexS.length==0) {
			fnCallback(-1, "Neispravni parametri");
			return;
		}


		var ajax = new XMLHttpRequest();
		ajax.onreadystatechange = function() {
			if (ajax.status==200 && ajax.readyState==4) {
				fnCallback(null, ajax.responseText);
			} else if (ajax.readyState==4) {
				fnCallback(ajax.status, ajax.responseText);
			}
		}

		ajax.open("POST", "/izvjestaj", true);
		ajax.setRequestHeader('Content-Type', 'application/json');
		ajax.send(JSON.stringify({spirala:spirala, index:index}));
	});

	kreirajBodove = (function(spirala, index, fnCallback) {
		var spiralaS="";
		var indexS="";
		if (typeof(spirala)!='string') {
			 spiralaS=spirala.toString();
		} else {
			spiralaS=spirala;
		}
		if (typeof(index)!='string') {
			indexS=index.toString();
		} else {
			indexS=index;
		}

		if (spiralaS.length==0 || indexS.length==0) {
			fnCallback(-1, "Neispravni parametri");
			return;
		}

		var ajax = new XMLHttpRequest();
		ajax.onreadystatechange = function() {
			if (ajax.status==200 && ajax.readyState==4) {
				fnCallback(null, ajax.responseText);
			} else if (ajax.readyState==4) {
				fnCallback(ajax.status, ajax.responseText);
			}
		}

		ajax.open("POST", "/bodovi", true);
		ajax.setRequestHeader('Content-Type', 'application/json');
		ajax.send(JSON.stringify({spirala:spirala, index:index}));
	});

	kreirajSpisak = (function(){
		var unos = document.getElementById('unos').value;
		var spirala = (document.getElementById('spirala').value).toString();
		var ajax = new XMLHttpRequest();

		var redovi = unos.split('\n');

		ajax.onreadystatechange = function() {
			if (ajax.readyState==4 && ajax.status==200) {
				document.getElementById('PObavijest').innerHTML=("Obavijest: Error - null"+"\nData - "+ajax.responseText);
			} else if (ajax.readyState==4) {
				document.getElementById('PObavijest').innerHTML=("Obavijest: Error - "+ajax.status+"\nData - "+ajax.responseText);
			}
		}
		ajax.open("POST", '/unosspiska', true);
		ajax.setRequestHeader('Content-Type', 'application/json');
		ajax.send(JSON.stringify({spirala: spirala, redovi: redovi}));
	
	});

    return {
        kreirajKomentar : kreirajKomentar,
        kreirajListu : kreirajListu,
        kreirajIzvjestaj : kreirajIzvjestaj,
		kreirajBodove : kreirajBodove,
		kreirajSpisak : kreirajSpisak    
    }
}());
