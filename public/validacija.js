var Validacija = (function() {
	var maxGrupa=7;
	var trenutniSemestar = 0;

		var validirajFakultetski = function(mail) {
			var reg = /(\w+@etf\.unsa\.ba)$/;
			var test = mail.split('@');
			if (test.length!=2) {
				Poruke.dodajPoruku(0);
				Poruke.ispisiGreske();
				return false;
			}
			if (!reg.test(mail)) {
				Poruke.dodajPoruku(0);
				Poruke.ispisiGreske();
				return false;
			}
			Poruke.ocistiGresku(0);
			Poruke.ispisiGreske();
			return true;
		}

		var validirajIndeks = function(indeks) {
			var reg = /1[0-9]{4}$/;
			if (reg.test(indeks) && indeks.length==5) {
				Poruke.ocistiGresku(1);
				Poruke.ispisiGreske();
				return true;
			}
			Poruke.dodajPoruku(1);
			Poruke.ispisiGreske();
			return false;
		}

		var validirajGrupu = function(broj) {
			var reg = /[0-9]/;
			if (!reg.test(broj)){
			Poruke.dodajPoruku(2);
			Poruke.ispisiGreske();
			return false;
			}
			if( (broj>=1) && (broj<=maxGrupa)) {
				Poruke.ocistiGresku(2);
				Poruke.ispisiGreske();
				return true;
			}
			Poruke.dodajPoruku(2);
			Poruke.ispisiGreske();
			return false;
		}

		var validirajAkGodinu = function(godina) {

			trenutniSemestar=document.getElementById("semestar").value;

			var reg = /^([0-9]{4})\/([0-9]{4})$/;

			if (!reg.test(godina)) {
				Poruke.dodajPoruku(3);
				Poruke.ispisiGreske();
				return false;
			}

			if (trenutniSemestar==0) {
				if (!((godina[0])+(godina[1])+(godina[2])+(godina[3])==(new Date()).getFullYear())) {
					Poruke.dodajPoruku(3);
					Poruke.ispisiGreske();
					return false;
				}
			} else {
				if (!((godina[5])+(godina[6])+(godina[7])+(godina[8])==(new Date()).getFullYear())) {
					Poruke.dodajPoruku(3);
					Poruke.ispisiGreske();
					return false;
				}
			}

			if(parseInt(godina[2])*10+parseInt(godina[3])+1==parseInt(godina[7])*10+parseInt(godina[8])) {
				Poruke.ocistiGresku(3);
				Poruke.ispisiGreske();
				return true;
			}
			Poruke.dodajPoruku(3);
			Poruke.ispisiGreske();
			return false;
		}

		var validirajPassword = function(pass) {
			var reg = /(\w+[A-Z]+\w)|([A-Z]+\w)|(\w+[A-Z])/;
			var imaVeliko = reg.test(pass);
			var reg2=/(\w+[a-z]+\w)|([a-z]+\w)|(\w+[a-z])/;
			var imaMalo = reg2.test(pass);
			var reg3 = /(\w+[0-9]+\w)|([0-9]+\w)|(\w+[0-9])/;
			var imaBroj=reg3.test(pass);

			if( pass.length<=20 && pass.length>=7 && imaVeliko && imaMalo && imaBroj) {
				Poruke.ocistiGresku(4);
				Poruke.ispisiGreske();
				return true;
			}
			Poruke.dodajPoruku(4);
			Poruke.ispisiGreske();
			return false;

		}

		var validirajPotvrdu = function(pass, potvrda) {
			if(pass==potvrda) {
				Poruke.ocistiGresku(5);
				Poruke.ispisiGreske();
				return true;
			}
			Poruke.dodajPoruku(5);
			Poruke.ispisiGreske();
			return false;

		}

		var validirajBitbucketURL = function(url) {
			var reg = /^https:\/\/.*@bitbucket\.org\/.*\/.*\.git/;
			if(reg.test(url)) {
				Poruke.ocistiGresku(6);
				Poruke.ispisiGreske();
				return true;
			}
			Poruke.dodajPoruku(6);
			Poruke.ispisiGreske();
			return false;
		}

		var validirajBitbucketSSH = function(ssh) {
			var reg = /^git@bitbucket\.org:+\w+\/+\w+\.git/;
			if( reg.test(ssh)){
				Poruke.ocistiGresku(7);
				Poruke.ispisiGreske();
				return true;
			}
			Poruke.dodajPoruku(7);
			Poruke.ispisiGreske();
			return false;
		}

		var validirajNazivRepozitorija = function(r, naziv) {
			if (r==null) {
				r=/^(wtProjekat|wtprojekat)([0-9]{5})$/;
			}
			if(r.test(naziv)) {
				Poruke.ocistiGresku(8);
				Poruke.ispisiGreske();
				return true;
			}
			Poruke.dodajPoruku(8);
			Poruke.ispisiGreske();
			return false;
		}

		var validirajImeiPrezime = function(ip) {
			var ime=ip.split(" ");
			for (var i=0; i<ime.length; i++) {
				var regPrvoSlovo = /[A-Z]|Č|Ć|Đ|Š|Ž/;
				var ostatak = ime[i].substring(1,ime[i].length);
				var temp=ime[i];
				var reg=/[a-z]|č|ć|ž|š|đ|'|-|/;
				if (!(reg.test(ostatak) && regPrvoSlovo.test(temp[0]) && temp.length>2 && ime[i].length<13)) {
					Poruke.dodajPoruku(9);
					Poruke.ispisiGreske();
					return false;
				}
			}
			Poruke.ocistiGresku(9);
			Poruke.ispisiGreske();
			return true;
		}

		return {
		validirajFakultetski : validirajFakultetski,
		validirajIndeks : validirajIndeks,
		validirajGrupu : validirajGrupu,
		validirajAkGodinu : validirajAkGodinu,
		validirajPassword : validirajPassword,
		validirajPotvrdu : validirajPotvrdu,
		validirajBitbucketURL : validirajBitbucketURL,
		validirajBitbucketSSH : validirajBitbucketSSH,
		validirajNazivRepozitorija : validirajNazivRepozitorija,
		validirajImeiPrezime : validirajImeiPrezime
	}

})();