function promijeniNaProfesora() {
	document.getElementById("registracijaS").style.display="none";
	document.getElementById("registracijaP").style.display="block";
	Poruke.postaviIdDiva("registracijaP");
	Poruke.ocistiGresku(1);
	Poruke.ocistiGresku(2);
	Poruke.ocistiGresku(3);
	Poruke.ocistiGresku(4);
	Poruke.ocistiGresku(5);
	Poruke.ocistiGresku(6);
	Poruke.ocistiGresku(7);
	Poruke.ocistiGresku(8);
	Poruke.ocistiGresku(9);
	document.getElementById("greskeP").innerHTML="";
	document.getElementById("imeiprezimeP").value="";
}

function promijeniNaStudenta() {
	document.getElementById("registracijaP").style.display="none";
	document.getElementById("registracijaS").style.display="block";
	Poruke.postaviIdDiva("registracijaS");
	Poruke.ocistiGresku(1);
	Poruke.ocistiGresku(2);
	Poruke.ocistiGresku(3);
	Poruke.ocistiGresku(4);
	Poruke.ocistiGresku(5);
	Poruke.ocistiGresku(6);
	Poruke.ocistiGresku(7);
	Poruke.ocistiGresku(8);
	Poruke.ocistiGresku(9);
	document.getElementById("greskeS").innerHTML="";
}

function dodajStudenta(){
	let ajax = new XMLHttpRequest();
	// izbrisi ispod
	ajax.onreadystatechange = function(){
		if (ajax.readyState == 4 && ajax.status == 200){
	  	 	let data = JSON.parse(ajax.responseText);
	  	 	if(data.validnost){
	  	 		alert(data.poruka);
	  	  		location.reload(true);
	  	 	}
	  	  	else {
	  	  		alert(data.poruka);
	  	  	}
  	  	 }
  	  	 else if(ajax.readyState == 4){
  	  	 	let data = JSON.parse(ajax.responseText);
	  	 	if(data.validnost){
	  	 		alert(data.poruka);
	  	  		location.reload(true);
	  	 	}
	  	  	else {
	  	  		alert(data.poruka);
	  	  	}
  	  	 }
	  	  		
	};


	var imeS = document.getElementById('imeiprezime').value;
	var indeksS=document.getElementById('indeks').value;
	var mail=document.getElementById('email').value;
	//kako dobiti grupu?
	var e = document.getElementById("odaberiGrupu");
	var odabrano = e.options[e.selectedIndex].value;

	var pass=document.getElementById('passwordS').value;
	var potvrda=document.getElementById('passwordPotvrdaS').value;
	var bburl=document.getElementById('bburl').value;
	var bbssh=document.getElementById('bbssh').value;
	var rep=document.getElementById('repozitorij').value;
	var godina = document.getElementById('akgodina').value;



	ajax.open("POST", "/studentRegistracija", true);
	ajax.setRequestHeader("Content-Type", "application/json");
	ajax.send(JSON.stringify({ime: imeS, indeks: indeksS, email:mail, password:pass, passwordPotvrda:potvrda, bburl:bburl, bbssh:bbssh, repozitorij:rep, akgodina:godina, odaberiGrupu:odabrano}));
}
function prijavi(){
	let ajax = new XMLHttpRequest();
	ajax.open("POST", "/prijavi", true);
	ajax.setRequestHeader("Content-Type", "application/json");
	ajax.send(JSON.stringify({username: document.getElementById('loginUsername').value, password: document.getElementById('loginPass').value}));
	// izbrisi ispod
	ajax.onreadystatechange = function(){
		if (ajax.readyState == 4 && ajax.status == 200){
	  		let response = JSON.parse(ajax.responseText);
	  		alert(response.poruka);
	  		location.reload();
	  	}

	  	  		
	};
}

function odjavi(){
	let ajax = new XMLHttpRequest();
	ajax.open("GET", "/odjavi", true);
	ajax.setRequestHeader("Content-Type", "application/json");
	ajax.send();	// izbrisi ispod
	ajax.onreadystatechange = function(){
		if (ajax.readyState == 4 && ajax.status == 200){
	  		let response = JSON.parse(ajax.responseText);
	  		alert(response.poruka);
	  		location.reload();
	  	}

	  	  		
	};
}

function dodajProfu(){
	let ajax = new XMLHttpRequest();
	// izbrisi ispod
	ajax.onreadystatechange = function(){
		if (ajax.readyState == 4 && ajax.status == 200){
	  	 	let data = JSON.parse(ajax.responseText);
	  	 	if(data.validnost){
	  	 		alert(data.poruka);
	  	  		location.reload(true);
	  	 	}
	  	  	else {
	  	  		alert(data.poruka);
	  	  	}
  	  	 }
  	  	 else if(ajax.readyState == 4){
  	  	 	let data = JSON.parse(ajax.responseText);
	  	 	if(data.validnost){
	  	 		alert(data.poruka);
	  	  		location.reload(true);
	  	 	}
	  	  	else {
	  	  		alert(data.poruka);
	  	  	}
  	  	 }
	  	  		
	};


	var imeP = document.getElementById('imeiprezimeP').value;
	var mail=document.getElementById('emailP').value;
	//kako dobiti grupu?
	var e = document.getElementById("semestar");
	var odabrano = e.options[e.selectedIndex].value;

	var pass=document.getElementById('passwordP').value;
	var potvrda=document.getElementById('passwordPotvrdaP').value;
	var korisIme=document.getElementById('kime').value;
	var brojG=document.getElementById('maxbroj').value;
	var regex = document.getElementById('regex').value;
	var godina = document.getElementById('akgodinaP').value;


	ajax.open("POST", "/profesorRegistracija", true);
	ajax.setRequestHeader("Content-Type", "application/json");
	ajax.send(JSON.stringify({ime: imeP, email:mail, password:pass, passwordPotvrda:potvrda, akgodina:godina, semestar:odabrano, korisnickoIme: korisIme, maxBrojGrupa:brojG, regex:regex}));
}