var Poruke = (function() {

	var idDivaPoruka="registracijaS";
	var mogucePoruke=["Email koji ste napisali nije validan fakultetski email.<br>",
						"Indeks kojeg ste napisali nije validan.<br>",
						"Nastavna grupa koju ste napisali nije validna.<br>",
						"Akademska godina koju ste napisali nije validna.<br>",
						"Password koji ste ukucali nije validan.<br>",
						"Potvrda i password se ne slazu.<br>",
						"Bitbucket URL nije validan.<br>",
						"Bitbucket SSH nije validan.<br>",
						"Naziv repozitorija koji ste unijeli nije validan.<br>",
						"Ime i prezime nije validno.<br>"];
	var porukeZaIspis=["","","","","","","","",""];

	var ispisiGreske = (function() {
		if (idDivaPoruka=="registracijaS") {
			document.getElementById('greskeS').innerHTML="";
			for (var i=0; i<porukeZaIspis.length; i++) {
				document.getElementById('greskeS').innerHTML+=porukeZaIspis[i];
			}
		} else {
			document.getElementById('greskeP').innerHTML="";
			for (var i=0; i<porukeZaIspis.length; i++) {
				document.getElementById('greskeP').innerHTML+=porukeZaIspis[i];
			}
		}
	});
	var postaviIdDiva = (function(iddiv) {
		idDivaPoruka=iddiv;
	});
	var dodajPoruku = (function(broj) {
		porukeZaIspis[broj]=(mogucePoruke[broj]);
	});
	var ocistiGresku = (function(broj) {
		porukeZaIspis[broj]="";
	});

	return {
		ispisiGreske : ispisiGreske,
		postaviIdDiva : postaviIdDiva,
		dodajPoruku : dodajPoruku,
		ocistiGresku : ocistiGresku
	}

}());