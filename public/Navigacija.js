var Navigacija = (function() {
    idiNaLogin = (function(){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            let sadrzaj = document.getElementById('kontejner');
            if (ajax.readyState == 4 && ajax.status == 200) {
                sadrzaj.innerHTML = ajax.responseText;
            } else if (ajax.readyState==4) {
                sadrzaj.innerHTML=ajax.responseText;
            }
        }
        ajax.open("GET", "/login", true);
        ajax.send();
    });

    idiNaStatistiku = (function(){
        var ajax = new XMLHttpRequest();
            ajax.onreadystatechange = function() {
                let sadrzaj = document.getElementById('kontejner');
                if (ajax.readyState == 4 && ajax.status == 200) {
                    sadrzaj.innerHTML = ajax.responseText;
                } else if (ajax.readyState==4) {
                    sadrzaj.innerHTML=ajax.responseText;
                }
            }
            ajax.open("GET", "statistika", true);
            ajax.send();
    });

    idiNaUnosKomentara = (function(){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            let sadrzaj = document.getElementById('kontejner');
            if (ajax.readyState == 4 && ajax.status == 200) {
                sadrzaj.innerHTML = ajax.responseText;
            } else if (ajax.readyState==4) {
                sadrzaj.innerHTML=ajax.responseText;
            }
        }
        ajax.open("GET", "unoskomentara", true);
        ajax.send();
    });

    idiNaUnosSpiska = (function(){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            let sadrzaj = document.getElementById('kontejner');
            if (ajax.readyState == 4 && ajax.status == 200) {
                sadrzaj.innerHTML = ajax.responseText;
            } else if (ajax.readyState==4) {
                sadrzaj.innerHTML=ajax.responseText;
            }
        }
        ajax.open("GET", "unosSpiska", true);
        ajax.send();
    });

    idiNaNastavnik = (function(){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            let sadrzaj = document.getElementById('kontejner');
            if (ajax.readyState == 4 && ajax.status == 200) {
                sadrzaj.innerHTML = ajax.responseText;
            } else if (ajax.readyState==4) {
                sadrzaj.innerHTML=ajax.responseText;
            }
        }
        ajax.open("GET", "nastavnik", true);
        ajax.send();
    });

    idiNaBBpozivi = (function(){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            let sadrzaj = document.getElementById('kontejner');
            if (ajax.readyState == 4 && ajax.status == 200) {
                sadrzaj.innerHTML = ajax.responseText;
            } else if (ajax.readyState==4) {
                sadrzaj.innerHTML=ajax.responseText;
            }
        }
        ajax.open("GET", "BBpozivi", true);
        ajax.send();
    });

   

     idiNaAdministraciju = (function(){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            let sadrzaj = document.getElementById('kontejner');
            if (ajax.readyState == 4 && ajax.status == 200) {
                sadrzaj.innerHTML = ajax.responseText;
            } else if (ajax.readyState==4) {
                sadrzaj.innerHTML=ajax.responseText;
            }
        }
        ajax.open("GET", "Administracija", true);
        ajax.send();
    });

    return {
        idiNaLogin :idiNaLogin,

        idiNaStatistiku : idiNaStatistiku,

        idiNaUnosKomentara : idiNaUnosKomentara,

        idiNaUnosSpiska : idiNaUnosSpiska,

        idiNaNastavnik : idiNaNastavnik,

        idiNaBBpozivi : idiNaBBpozivi,
        idiNaAdministraciju: idiNaAdministraciju
    }
}());
