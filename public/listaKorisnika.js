function pretrazi(){
	var pretraga =  document.getElementById('poljeZaUnos').value;
	var rezultat = document.getElementById('rezultatiPretragaKorisnika');
	var sadrzaj = "";
	var	ajax = new XMLHttpRequest();

	ajax.onreadystatechange = function(){
		if (ajax.readyState == 4 &&	ajax.status == 200){
	  		// korisnik model p + button
	  		var data = JSON.parse(ajax.responseText);
	  		console.log(ajax.responseText);

	  		var temp = "<span>"+data['korisnik'];
	  		if (data['verif']==false) {
	  			temp += '<button type="button" onclick="verify('+data['ID']+')">Verifikuj</button></span>';
	  		} else if (data['verif']==true) {
				temp += '<button type="button" onclick="unverify('+data['ID']+')">Unverifikuj</button></span>';
	  		} else{
	  			temp+='</span>';
	  		}

	  		rezultat.innerHTML = temp;

	  	}	  		
	};


	ajax.open("POST", "/pretrazi", true);
	ajax.setRequestHeader("Content-Type", "application/json");
	ajax.send(JSON.stringify({polje: pretraga}));

}



function verify(id){
	console.log("TU SAM");
	var ajax = new XMLHttpRequest();

	ajax.onreadystatechange = function() {
		if (ajax.readyState == 4 &&	ajax.status == 200){
	  		alert("Uspjesno!");
	  	} else {
	  	}
	};

	ajax.open("POST", "/verifikuj", true);
	ajax.setRequestHeader("Content-Type", "application/json");
	ajax.send(JSON.stringify({ID: id}));

}



function unverify(id){
	var ajax = new XMLHttpRequest();

	ajax.onreadystatechange = function() {
		if (ajax.readyState == 4 &&	ajax.status == 200){
	  		alert("Uspjesno!");
	  	} else {
	  	}
	};

	ajax.open("POST", "/unverifikuj", true);
	ajax.setRequestHeader("Content-Type", "application/json");
	ajax.send(JSON.stringify({ID: id}));
}